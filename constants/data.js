const userData = {
  githubUsername: "mukesh-theavatar",
  name: "Mukesh Kr. Ranjan",
  designation: "Senior Technology Officer",
  avatarUrl: "/avatar.jpeg",
  email: "mukesh.ranjan593@gmail.com",
  phone: "+91 7260095190",
  address: "Kolkata, West Bengal, India.",
  projects: [
    {
      title: "Fun Walking club",
      link: "https://funwalking.club/",
      imgUrl: "/fwc.png",
    },
    {
      title: "cre8 - iprototechnology",
      link: "https://www.cre8.zone/",
      imgUrl: "/cre8.png",
    },
    {
      title: "WebDevPool",
      link: "https://webdevpool.herokuapp.com/",
      imgUrl: "/webdevpool.png",
    },
    {
      title: "Tic Tac Toe game",
      link: "https://mukesh-theavatar.github.io/GridGame/",
      imgUrl: "/tictactoe.png",
    },
    {
      title: "Advika Enterprises",
      link: "https://myadvika.com/",
      imgUrl: "/myadvika.png",
    },
    {
      title: "EdElevate(Formerly ITP Study Abroad)",
      link: "https://edelevate.com/",
      imgUrl: "/edelevate-itpstudyabroad.png",
    },
        {
      title: "Portfolio",
      link: "https://github.com/mukesh-theavatar",
      imgUrl: "/portfolio-demo.png",
    },
    {
      title: "ItzEazy",
      link: "https://www.itzeazy.com/",
      imgUrl: "/itzeazy.png",
    },
  ],
  about: {
    title:
      "I'm a software developer who loves building products and web applications that impact millions of lives",
    description: [
      `I've been developing Web applications and Websites since I was 21 years old. I didn't know what full-stack meant at that time because the term was not coined back then. I'm so out of content right now that I'm literally crying writing this text since then and its 2022 now.`,
      `After learning HTML and struggling with CSS, I came up with a brilliant idea of using bootstrap so that I don't have to style everything by myself and - for obvious reasons - if you knew bootstrap, you were cool.`,
      `But now, I'm a expert. I've been coding in Node, JavaScript, TypeScript, Next.js, Node.js, Express, Lamda, MongoDB, MySQL, Bootstrap, Tailwind, C++, Core Java, Shell programming language etc to name a few. Although I barely know the syntax (Google, Stack overflow!, Medium etc.), I consider myself a Ninja developer I'm laughing right now.`,
    ],
    currentProject: "Fun Walking Club",
    currentProjectUrl: "https://funwalking.club/",
  },
  experience: [
    {
      title: "Process Engineer & Code Development Lead - Backend",
      company: "FreeFlow Venture Builders, Kolkata",
      year: "2021-Ongoing",
      companyLink: "https://www.freeflow.zone/",
      desc: "Contributed to a Several Client & Internal Projects - Fun Walking Club - Cre8 ProtoType Technology - UpFlow Platform - Mastermentors - FaceLabs, BlueMedix etc.",
    },
    {
      title: "Chief Technology Officer & Node.js Developer ",
      company: "WebDevPool Technologies",
      year: "2020-2021",
      companyLink: "https://webdevpool.herokuapp.com/",
      desc: "Developed a Community for software developers & helping recuruiters to hire best developers around the world. Built on top of (Express, React, MongoDB and Node.js) and Gravatar profile.",
    },
    // {
    //   title: "Founder and CTO",
    //   company: "iplaycode Platform",
    //   year: "2021-Ongoing",
    //   companyLink: "http://iplaycode.online",
    //   desc: "Learn coding online while solving real life problems and build solutions in guidance with experts. Practice problems and get hired at top Product based companies.",
    // },
    {
      title: "Software Developer - MERN Stack ",
      company: "Freelance - Fiverr",
      year: "2020-2021",
      companyLink: "https://www.fiverr.com/",
      desc: "Contributed to a Client product - Brihha LMS - which is an Learning Management Sytem for programmers and students.",
    },
    {
      title: "Product Engineering Course - Coding Bootcamp",
      company: "SOAL, Hyderabad",
      year: "2020",
      companyLink: "https://www.schoolofacceleratedlearning.com/",
      desc: "SOAL curriculum taught me with the required set of tools and skill set to be a software developer in MERN Stack - Mongo DB, Express.js, React.Js and Node.js.",
    },
    {
      title: "Senior Advisor",
      company: "Concentrix Daksh Services India Private Limted",
      year: "2015-2020",
      companyLink: "https://www.concentrix.com/",
      desc: "Worked for Client Google & Youtube for various internal tech & semi-technical projects to name a few are Buganizer Tool, Revue Tool, Youtube Machine Learning Projects ",
    },

    {
      title: "SEO & Data Analyst",
      company: "Innovative Training Place Private Limited",
      year: "2014-2015",
      companyLink: "https://edelevate.com",
      desc: "Work on web development for itpstudyaboard.com website - designed website wireframing from scratch and handled the database. Also, Responsible for managing client websites & IT Support! ",
    },

    {
      title: "Internship",
      company: "ItzEazy Services India Private Limited",
      year: "2014",
      companyLink: "https://itzeazy.com",
      desc: "Work on web development for itzeazy website and apply search engine optimization techniques to achieve organic growth.",
    },
    {
      title: "Graduation",
      company: "I. K. Gujral Punjab Technical University, Jalandhar.",
      year: "2014",
      companyLink: "https://ptu.ac.in/",
      desc: "Major in Computer Science and Engineering with a CGPA of 8.0. Nobody asks this but it's okay.",
    },
    {
      title: "High School",
      company: "H.A.V High School, Bokaro.",
      year: "2007",
      companyLink: "https://jac.jharkhand.gov.in/jac/",
      desc: "Subjects being English, Maths, Hindi, Science, Khortha, Social Science - I barely survived with 75% marks aggregate. Flex fridays, fellas.",
    },
  ],
  resumeUrl:
    "https://drive.google.com/file/d/1Zd0AoP0O5c_TQtQi5Wy-o-aV3lihiu0p/view",
  socialLinks: {
    instagram: "https://instagram.com/",
    twitter: "https://twitter.com/imukeshr",
    linkedin: "https://www.linkedin.com/in/mukesh-kumar-ranjan-4263568b/",
    github: "https://github.com/mukesh-theavatar",
    facebook: "https://facebook.com/imukeshr",
  },
};

export default userData;
